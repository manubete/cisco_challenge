# README.md


### Setup


Install python3

Install pip3

Install virtualenv (recommended with pip3)
	
From directory above current one, activate virtual environment
	
```
virtualenv cisco_url_shortener

source cisco_url_shortener/bin/activate
```

Navigate to project directory

```
cd cisco_url_shortener
```

Install Django

```
pip3 install django
```

Either run migrations or specify migrations if need

```
python manage.py migrate
```

Or

```
python manage.py makemigrations shortUrls

python manage.py migrate
```

Next populate phishing Urls with most recently downloaded version

```
python3 populate_phishing_data.py
```

### General Comments and progress

The current iteration of the URL shortener satisfies all the specs specified in the email. The interactions follow the 6 specified steps, there is a check against Phishing URL's incorporated into the app and there is more code than just CSS for the frontend. Both the population script and the shorturl creation do not create duplicate entries, all entries are stored and have no set TTL

Ideally I would have made a lot of improvements in a lot of areas. For starters I would have incorporated a NoSQL database (like MongoDB) instead of the standard django database (SQLite 3). I mostly opted to use the default to make my setup as smoothly as possible, I played around with incorporating different databases but it was too much of a timesink given the time I had available for the project. A NoSQL database would have helped with performance since the app itself at this point only uses very simple reads and writes doesnt require many calls on related objects save checking for PhishUrl's. 

Another optimization would have been a beefier hash function, I open sourced a hash library but I would have liked to do a base 30 or so hash where I could have used different symbols and alphanumeric characters to create the shortened URL slugs.
Again this was to optimize for the app creation given my timeline.

Other optimizations I can think of would along the lines of creating analytics for the URL's such as keeping track of when and how many times URL's are visited. Other useful analytics could be conversions of when a safe url turned into a dangerous URL and as such keep a more dynamic blacklist of phishing URL's.

Other optimizations could have been other layers of security around the URL, I already am checking both the phishing URL's as well as their netloc (hostname and domain name) as an extra precaution however I could imagine other optimizations such as using user authentication to prevent external non authorized users to access an endpoint of a shortUrl that could lead them to a private endpoint encoded by the shortURL service.
