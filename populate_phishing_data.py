# set up django environment for the script
import csv
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cisco_url_shortener.settings")
django.setup()

#populating script
from urllib.parse import urlparse
from shortUrls.models import ShortUrl, PhishUrl
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

with open('verified_online.csv',newline='') as csvfile:
	reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
	for row in reader:
		url = row[0].split(",")[1]
		phishUrl = None
		parsedUrl = None
		try:
			parsedUrl = urlparse(url)
			phishUrl = PhishUrl.objects.get(phishUrl=url)
		except ValidationError:
			print("Not a valid URL")
		except PhishUrl.DoesNotExist:
			netloc = parsedUrl.netloc
			phishUrl = PhishUrl(phishUrl=url, netloc=netloc)
			phishUrl.save()

