import hashlib

from urllib.parse import urlparse

from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect
from django.http import HttpResponse

from .models import ShortUrl, PhishUrl
from .forms import ShortUrlForm


BASE_URL = 'http://localhost:8080/shortUrls/'

def newLink(request):
	if request.method == "POST":
		form = ShortUrlForm(request.POST)
		if form.is_valid():
			shortUrl = form.save(commit=False)
			# check if Url is valid
			try:
				parsedUrl = urlparse(shortUrl.original_url)
			except ValidationError:
				parsedUrl = None
				form = ShortUrlForm()
				warning_message = "URL is not valid"
				return render(request, 'shortUrls/newLink.html', 
					{'form': form, 'warning_message': warning_message})

			# check if Url already exists with checkUrl
			try:
				checkUrl = ShortUrl.objects.get(original_url=shortUrl.original_url)
			except ShortUrl.DoesNotExist:
				checkUrl = None

			if checkUrl:
				shortUrl_id = checkUrl.pk
				return redirect('detail', shortUrl_id=shortUrl_id )

			# check if Url is used for phishing (url or netloc), and if url is valid
			try:
				if parsedUrl:
					netloc = parsedUrl.netloc
					netlocUrl = PhishUrl.objects.filter(netloc=netloc)
					if netlocUrl.count() >= 1:
						netlocUrl = netlocUrl.first()
					else:
						netlocUrl = None

				phishUrl = PhishUrl.objects.get(phishUrl=shortUrl.original_url)
			except PhishUrl.DoesNotExist:
				phishUrl = None
				netlocUrl = None

			if phishUrl or netlocUrl:
				phishUrl = phishUrl.phishUrl
				form = ShortUrlForm()
				warning_message = "was identified as Phish URL"
				return render(request, 'shortUrls/newLink.html', 
					{'form': form, 'warning_message': warning_message, 'phishUrl': phishUrl})
			else:
				hashed_url = hashlib.md5(shortUrl.original_url.encode()).hexdigest()
				shortUrl.shortened_url = ''.join(list(hashed_url)[0:10])
				shortUrl.save()
				shortUrl_id = shortUrl.pk
				return redirect('detail', shortUrl_id=shortUrl_id )
	else:
		form = ShortUrlForm()
		return render(request, 'shortUrls/newLink.html', {'form': form})

def detail(request, shortUrl_id):
	shortUrl = ShortUrl.objects.get(id=shortUrl_id).shortened_url
	redirect_url = BASE_URL + shortUrl
	return render(request, 'shortUrls/detail.html',
		{'redirect_url': redirect_url, 'shortUrl': shortUrl })

def shortUrlRedirect(request, shortUrl_shortened_url):
	original_destination_url = ShortUrl.objects.get(shortened_url= shortUrl_shortened_url).original_url
	return redirect(original_destination_url)