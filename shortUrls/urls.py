from django.urls import path

from . import views

urlpatterns = [
    path('', views.newLink, name='newLink'),
    path('<int:shortUrl_id>/', views.detail, name='detail'),
    path('<str:shortUrl_shortened_url>/', views.shortUrlRedirect, name='shortUrlRedirect')
]