from django.apps import AppConfig


class ShorturlsConfig(AppConfig):
    name = 'shortUrls'
