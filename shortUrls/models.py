from django.db import models

# Create your models here.
class ShortUrl(models.Model):
	original_url = models.CharField(max_length=800)
	shortened_url = models.CharField(max_length=100)

class PhishUrl(models.Model):
	phishUrl = models.CharField(max_length=800)
	netloc = models.CharField(max_length=300)
